OUT := forth86.bin

SRC := boot.s
DEP := deps.d

$(OUT): $(SRC)
	nasm $(SRC) -o $(OUT) -MD $(DEP)

.PHONY: clean run

clean:
	rm -f $(OUT)

run: $(OUT)
	qemu-system-x86_64 -drive file=$(OUT),format=raw

-include $(DEP)
