# Forth86
## Bare metal x86 forth

This is a sort of hodgepodge attempt at a forth system using everything I know
about forth and a lot of online resources. I wanted it to run on bare metal, as
forth seems to run best there. I'm not sure what pieces of it I'm going to
implement, and I'm sure it's not the simplest or most efficient implementation
out there. I'd avoid x86 if I could, as I find the architecture horrendously
complex. However, I can't get Pi Pico assemblers working properly, and this is
what I have. Hopefully it isn't too horribly out of control.

This system is probably going to turn into a mess. Suggestions welcome.
