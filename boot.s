; forth for x86
; honestly, x86 is awful, but it's what I have

[bits 16]
[org 0x7c00]
real:
	mov bp, 0x9000 ; set stack
	mov sp, bp

	call enter_pm ; never return

; GDT - why is this so awful
gdt_start:
gdt_null:
	dd 0x0 ; 
	dd 0x0 ; define 8 bytes of nothing
gdt_code: ; code segment descriptor
	; base 0x0, limit 0xfffff
	; 1st flags: (present)1 (privelege)00 (descriptor-type)1 -> 1001b
	; type flags: (code)1 (conforming)0 (readable)1 (accessed)0 -> 1010b
	; 2nd flags: (granularity)1 (32-bit)1 (64-bit)0 (AVL)0 -> 1100b
	dw 0xffff ; limit (0:15)
	dw 0x0 ; base (0-15)
	db 0x0 ; base (16-23)
	db 10011010b ; 1st flags, type flags
	db 11001111b ; 2nd flags, limit (16:19)
	db 0x0 ; base (23:31)
gdt_data: ; data segment descriptor
	; same as code except for type flags
	; type flags: (code)0 (expand-down)0 (writable)1 (accessed)0 -> 0010b
	dw 0xffff ; limit (0:15)
	dw 0x0 ; base (0-15)
	db 0x0 ; base (16-23)
	db 10010010b ; 1st flags, type flags
	db 11001111b ; 2nd flags, limit (16:19)
	db 0x0 ; base (23:31)
gdt_end: ; nasm can calculate the size of the GDT

gdt_descriptor:
	dw gdt_end - gdt_start - 1 ; size of GDT - 1
	dd gdt_start ; GDT address

CODE_SEG equ gdt_code - gdt_start ; GDT code segment address
DATA_SEG equ gdt_data - gdt_start

enter_pm:
	cli
	lgdt [gdt_descriptor] ; load our GDT

	mov eax, cr0
	or eax, 0x1 ; set cr0 first bit
	mov cr0, eax ; ^

	jmp CODE_SEG:init_pm ; far jump to init to clear pipeline

[bits 32]
init_pm:
	mov ax, DATA_SEG
	mov ds, ax
	mov ss, ax
	mov es, ax
	mov fs, ax
	mov gs, ax

	; we're gonna adopt sector forth's approach to stacks
	; I don't know much x86 assembly, so its very likely that there is a faster way to do this
	mov ebp, 0x90000 ; return stack location
	mov esp, 0xa0000 ; data stack
	; hopefully no one's stack is bigger than 64KiB

	call start

; that's all the boilerplate, time for some macros

; dictionary entry memory map
; *--------------*----------------*------*------*------------*
; | Link pointer | Flags / Length | Name | Code | Parameters |
; *--------------*----------------*------*------*------------*
;   4 bytes        1 byte          Length  Variable ------->

%define link 0 ; last dictionary entry links to 0

%macro defword 3 ; name, label, flags
word_%2: ; label
	dd link
%define link word_%2 ; make new link
%strlen len %1
	db %3 | len ; flags / len
	db %1 ; name
%2: ; code starts here
%endmacro

%macro NEXT 0
	lodsd
	jmp eax
%endmacro

%define DOCOL jmp docol

; restart interpreter at new word
docol:
	xchg esp, ebp ; use return stack
	push esi ; put address of next word on return stack
	xchg esp, ebp
	add eax, 5 ; THIS PROBABLY WILL NEED TO BE CHANGED, INSTRUCTIONS ARE WEIRD SIZES (looks like 5 bytes? works for now)
	mov esi, eax
	NEXT ; restart interpreter

; primitives
; arithmetic
defword "+", ADD, 0
	pop eax
	pop ebx
	add eax, ebx
	push eax
	NEXT
defword "-", SUB, 0
	pop eax
	pop ebx
	sub eax, ebx
	push eax
	NEXT
defword "*", MUL, 0
	pop eax
	pop ebx
	imul eax, ebx
	push eax
	NEXT

; stack manipulation
defword "dup", DUP, 0
	pop eax
	push eax
	push eax
	NEXT
defword "drop", DROP, 0
	pop eax
	NEXT
defword "swap", SWAP, 0
	pop eax
	pop ebx
	push eax
	push ebx 
	NEXT
defword "rot", ROT, 0
	pop eax
	pop ebx
	pop ecx
	push eax
	push ecx
	push ebx
	NEXT
; I'll probably need to add more
; interpreter stuff
; lets us reuse the interpreter
defword "exit", EXIT, 0
	xchg esp, ebp ; we're using the return stack now
	pop esi ; go back to old address list
	xchg esp, ebp
	NEXT

VIDEO_MEM equ 0xb8000

defword "code_test", TEST, 0
	DOCOL
	dd ADD
	dd SUB
	dd EXIT

test_run:
	dd TEST
	dd end_run

start: ; finally can start our code
	; clear video memory before starting
	mov ecx, VIDEO_MEM
.loop:
	mov dword [ecx], 0x0
	add ecx, 4
	cmp ecx, VIDEO_MEM + 80 * 25 * 2 ; 80x25, one word per character
	jl .loop
	; screen is now clear for our use

	push 7
	push 'a'
	push 18

	mov esi, test_run
	NEXT
end_run:
	; interpreter test

	; did add work?
	pop eax
	or ax, 0x0f00
	mov word [VIDEO_MEM], ax

	jmp $ ; hang

; padding
times 510-($-$$) db 0
dw 0xaa55
